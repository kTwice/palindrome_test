import language
import utils
import locale


class OHCE:

    def palindrome(self, string: str):
        depend_time_hello = utils.current_time()
        loc = locale.getlocale()

        if not isinstance(string, str):
            raise ValueError("vous devez mettre un mot")
        _reversed = string[::-1]

        if string.lower() == _reversed.lower():
            if loc[0] == "fr_FR":
                _reversed = _reversed + language.Francais.well_down
            else:
                _reversed = _reversed + language.English.well_down

        if loc[0] == "fr_FR":
            response = depend_time_hello + _reversed + language.Francais.goodbye
        else:
            response = depend_time_hello + _reversed + language.English.goodbye

        return response
