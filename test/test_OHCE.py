import unittest

import language as lang
import utils
import locale

from Model import OHCE
from unittest.mock import Mock

class TestOHCE(unittest.TestCase):

    def test_is_reversed(self):
        """lorsque l'on saisie une chaine de caractère celle ci est renvoyer en miroir"""
        # Etant donné l'OHCE
        ohce = OHCE()
        hello = utils.current_time()

        # quand on saisit une chaine,
        _in = "toto"

        # Alors celle-ci est renvoyée en miroir
        val_test = ohce.palindrome(_in)
        self.assertEqual(hello + "otot au revoir", val_test)

    def test_is_palindrome(self):
        """lorsque l'on saisie un palindrome celle ci renvoie le mot en mirroir suivit de bien jouer"""
        # Etant donné l'OHCE
        ohce = OHCE()
        hello = utils.current_time()

        # quand on saisit un plaindrome,
        _in = "radar"

        # Alors celui-ci est renvoyé en mirroir et bien jouer
        val_test = ohce.palindrome(_in)
        self.assertEqual(hello + "radar, bien jouer au revoir", val_test)

    def test_radar_lower_case(self):
        """ Test que OHCE renvoi radar et bien jouer peut import la case"""
        # Etant donné l'OHCE
        ohce = OHCE()
        hello = utils.current_time()

        # quand on saisit un plaindrome peut import la case,
        _in = "radar"
        _in2 = "Radar"

        # Alors celui-ci est renvoyé en mirroir et bien jouer
        self.assertEqual(hello + "radar, bien jouer au revoir", ohce.palindrome(_in))
        self.assertEqual(hello + "radaR, bien jouer au revoir", ohce.palindrome(_in2))

    def test_bonjour(self):
        """ Test l'existence du message de bonjour"""
        # Etant donné l'OHCE et l'utilitaire de gestion du temps
        ohce = OHCE()
        hello = utils.current_time()

        # quand on saisit une chaine de charactère,
        _in = "truc"

        # Alors celui-ci nous renvoie bonjour en début de réponse
        self.assertEqual(hello + "curt au revoir", ohce.palindrome(_in))

    def test_au_revoir(self):
        """ Test l'existence du message de au revoir"""
        # Etant donné l'OHCE et l'utilitaire de gestion du temps
        ohce = OHCE()
        hello = utils.current_time()

        # quand on saisit une chaine de charactère,
        _in = "truc"

        # Alors celui-ci nous renvoie au revoir en fin de réponse
        self.assertEqual(hello + "curt" + lang.Francais.goodbye, ohce.palindrome(_in))

    def test_dire_bonsoir(self):
        """ Test en fonction de la journée dit bonsoir au lieu de bonjour """
        # Etant donné l'OHCE et l'utilitaire de gestion du temps
        ohce = OHCE()
        hello = utils.current_time()

        # quand on saisit une chaine de charactère,
        _in = "test"

        # Alors celui-ci nous renvoie en fonction de l'heure de la journée un bonjour, bonsoir etc.. en début de réponse
        self.assertEqual(hello + "tset" + lang.Francais.goodbye, ohce.palindrome(_in))

    def test_version_international(self):
        """ Test ou la langue est paramêtrer en fonction de la localité du pc """
        # Etant donné l'OHCE et l'utilitaire de gestion du temps ainsi que la valeur de la langue local
        ohce = OHCE()
        hello = utils.current_time()
        loc = locale.getlocale()

        # quand on saisit une chaine de charactère,
        _in = "particular"

        # Alors celui-ci nous renvoie la reponse dans la langue défini dans l'OS Francais ou anglais
        if loc[0] == "fr_FR":
            self.assertEqual(hello + "ralucitrap" + lang.Francais.goodbye, ohce.palindrome(_in))
        elif loc[0] == "en_GB" or loc[0] == "en_US":
            self.assertEqual(hello + "ralucitrap" + lang.English.goodbye, ohce.palindrome(_in))

    """Test Recettes"""

    def test_automatic_palindrome_english_evening(self):
        """recette ou le scénario est le suivant : saisie automatique d'un palindrome en anglais durant la soirée
                POUR QUE LE TEST FONCTIONNE IL FAUT METTRE SON PC EN ANGLAIS"""
        # Etant donné l'OHCE et la valeur good evening
        ohce = OHCE()
        good_evening = "good evening "

        # quand on saisit un plaindrome,
        _in = "kayak"

        # Alors celui-ci nous renvoie la reponse en anglais commencant par good evening avec well down pour le
        # palindrome
        self.assertEqual(good_evening + "kayak" + lang.English.well_down + lang.English.goodbye, ohce.palindrome(_in))

    def test_automatic_none_palindrome_french_morning(self):
        """recette ou le scénario est le suivant : saisie automatique d'un mot en français durant la matinée
                POUR QUE LE TEST FONCTIONNE IL FAUT METTRE SON PC EN FRANCAIS"""
        # Etant donné l'OHCE et la valeur bonjour
        ohce = OHCE()
        bonjour = "bonjour "

        # quand on saisit n'importe quel mot,
        _in = "oula"

        # Alors celui-ci nous renvoie la reponse en français commencant par bonjour et sans le bien jouer
        self.assertEqual(bonjour + "aluo" + lang.Francais.goodbye, ohce.palindrome(_in))

    def test_free_input_language_and_time_machine(self):
        """recette ou le scénario est le suivant : saisie du client en console temps et langue du système"""
        # Etant donné l'OHCE et l'utilitaire de gestion du temps ainsi que la valeur de la langue local
        ohce = OHCE()
        hello = utils.current_time()
        loc = locale.getlocale()

        # quand le client saisit un mot manuellement
        def mock_return():
            return "kayak"

        _in = mock_return()[::-1]

        if loc[0] == "fr_FR":
            # Alors celui-ci nous renvoie la reponse en français
            self.assertEqual(hello + _in + lang.Francais.well_down + lang.Francais.goodbye, ohce.palindrome(mock_return()))

        # sinon si le client est anglais
        elif loc[0] == "en_GB" or loc[0] == "en_US":
            # Alors celui-ci nous renvoie la reponse en anglais
            self.assertEqual(hello + _in + lang.English.well_down + lang.English.goodbye, ohce.palindrome(mock_return()))

