import datetime as dt
import language as lang
import locale


def current_time():
    hour = dt.datetime.now().hour
    value: str = ""
    loc = locale.getlocale()

    if loc[0] == "fr_FR":
        if 0 <= hour < 7:
            value = lang.Francais.good_night
        if 7 <= hour < 12:
            value = lang.Francais.good_morning
        if 12 <= hour < 17:
            value = lang.Francais.hello
        if 15 <= hour < 24:
            value = lang.Francais.good_evening

    if loc[0] == "en_GB" or loc[0] == "en_US":
        if 0 <= hour < 7:
            value = lang.English.good_night
        if 7 <= hour < 12:
            value = lang.English.good_morning
        if 12 <= hour < 17:
            value = lang.English.hello
        if 15 <= hour < 24:
            value = lang.English.good_evening

    return value
