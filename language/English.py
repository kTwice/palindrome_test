from language.DefaultLanguage import DefaultLanguage


class English(DefaultLanguage):
    hello = "hello "
    good_morning = "good morning "
    good_evening = "good evening "
    good_night = "good night "
    goodbye = " goodbye"
    well_down = ", well down"
    input_client = "input your word : "
