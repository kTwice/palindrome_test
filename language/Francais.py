from language.DefaultLanguage import DefaultLanguage


class Francais(DefaultLanguage):
    hello = "bonjour "
    good_morning = "bonjour "
    good_evening = "bonne soirée "
    good_night = "bonne nuit "
    goodbye = " au revoir"
    well_down = ", bien jouer"
    input_client = "saisissez votre mot : "
