from abc import ABC


class DefaultLanguage(ABC):
    """
    abstract class implement value ennum values of words use in plaindrome app
    """
    hello: str
    good_morning: str
    good_evening: str
    good_night: str
    goodbye: str
    well_down: str
    input_client: str

